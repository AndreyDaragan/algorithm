#include <iostream>
#include <algorithm>
#include <numeric>
#include <functional>
#include <vector>
#include <list>
#include <iterator>

using std::cin;
using std::cout;
using std::endl;
using namespace std::placeholders;

/// предикат
bool isShorter(const std::string& str1, const std::string& str2)
{
	return str1.size() < str2.size();
}

bool isPrint(const std::string& str, size_t sz, size_t sx)
{
	return str.size() >= sz + sx;
}

int main()
{
	std::vector<int> vec{1, 2, 3, 4, 5, 5, 7, 8, 9};
	/// find - ищем значение в контейнере
	cout << "std::find" << endl;
	std::vector<int>::const_iterator it_find = std::find(vec.cbegin(), vec.cend(), 5);
	if(it_find == vec.cend())
		cout << "Numeric value "<< 5 << " not found!" << std::endl;
	cout << "Found numeric is " << *it_find << endl;
	/// count - ищем количество вхождений значения в контейнере
	cout << endl << "std::count" << endl;
	const int count_number = std::count(vec.cbegin(), vec.cend(), 5);
	cout << "Number: " << 5 <<  " count: " << count_number << endl;
	/// accumulate - суммирует элементы контейнера с исходным значением суммы
	cout << endl << "std::accumulate" << endl;
	const int sum = std::accumulate(vec.cbegin(), vec.cend(), 0);
	cout << "Sum: " << sum << endl;
	/// equal - сравнение контейнеров. Размер второго контейнера должен быть не меньше размера первого!
	cout << endl << "std::equal" << endl;
	std::vector<int> vec2{1, 2, 3, 4, 5, 5, 7, 8, 1};
	const bool is_equal = std::equal(vec.cbegin(), vec.cend(), vec2.cbegin());
	cout << "Vectors is equal: " << std::boolalpha << is_equal << endl;
	/// fill - заполняет 5 элементов вектора значением, но не добавляет элементы!
	cout << endl << "std::fill" << endl;
	std::fill(vec2.begin(), vec2.begin() + 5, 0);
	for(const auto& elem : vec2)
		cout << elem << " ";
	cout << endl;
	/// fill_n - заполняет n элементов вектора значением, но не добавляет элементы!
	cout << endl << "std::fill_n" << endl;
	std::fill_n(vec2.begin(), 5, 1);
	for(const auto& elem : vec2)
		cout << elem << " ";
	cout << endl;
	/// fill_n - добавляет 5 элементов со значением 3 в контейнер
	cout << endl << "std::fill_n" << endl;
	std::vector<int> vec3;
	std::fill_n(std::back_inserter(vec3), 5, 3);
	for(const auto& elem : vec3)
		cout << elem << " ";
	cout << endl;
	/// copy - копирует один диапазон в другой
	cout << endl << "std::copy" << endl;
	std::vector<int> vec4(9);
	std::copy(vec.cbegin(), vec.cend(), vec4.begin());
	for(const auto& elem : vec4)
		cout << elem << " ";
	cout << endl;
	/// replace - заменяет элемент со значением x, значением y
	cout << endl << "std::replace" << endl;
	std::replace(vec3.begin(), vec3.end(), 3, 5);
	for(const auto& elem : vec3)
		cout << elem << " ";
	cout << endl;
	/// replace_copy - создает копию исходной последовательности в новую, заменяя элемент со значением x, значением y
	cout << endl << "std::replace_copy" << endl;
	std::vector<int> vec5;
	std::replace_copy(vec3.begin(), vec3.end(), std::back_inserter(vec5), 5, 9);
	for(const auto& elem : vec5)
		cout << elem << " ";
	cout << endl;
	/// sort unique - сортировка элементов последовательности и удаление дубликатов
	cout << endl << "std::sort and std::unique" << endl;
	std::sort(vec2.begin(), vec2.end());
	for(const auto& elem : vec2)
		cout << elem << " ";
	cout << endl;
	auto it_unique = std::unique(vec2.begin(), vec2.end());
	vec2.erase(it_unique, vec2.end());
	for(const auto& elem : vec2)
		cout << elem << " ";
	cout << endl;
	/// stable_sort - сортировка в алфавитном порядке
	cout << endl << "std::stable_sort" << endl;
	std::vector<std::string> vec6{"As", "The", "Ended", "End"};
	size_t sz{2};
	std::sort(vec6.begin(), vec6.end(), isShorter);
	std::stable_sort(vec6.begin(), vec6.end());
	for(const auto& elem : vec6)
		cout << elem << " ";
	cout << endl;
	auto it_longer = std::find_if(vec6.cbegin(), vec6.cend(), [sz] (const std::string& str) {
		return str.size() > sz;
	});
	size_t count_words = vec6.cend() - it_longer;
	cout << "Count: " << count_words << endl;
	std::for_each(it_longer, vec6.cend(), [](const std::string& str){
		cout << str << " ";
	});
	cout << endl;
	/// lambda expressions
	cout << endl << "lambda expressions" << endl;
	size_t size{5};
	auto f1 = [size] () mutable {
		cout << "In lambda[=]: " << ++size << endl;
	};
	cout << "Out lambda[=]: " << size << endl;
	++size;
	cout << "Out lambda[=]: " << size << endl;
	f1();
	cout << "Out lambda[=]: " << size << endl;
	size_t ref_size{3};
	auto f2 = [&ref_size] () {
		cout << "In lambda[&]: " << ++ref_size << endl;
	};
	cout << "Out lambda[&]: " << ref_size << endl;
	++ref_size;
	cout << "Out lambda[&]: " << ref_size << endl;
	f2();
	cout << "Out lambda[&]: " << ref_size << endl;
	/// std::bind
	cout << endl << "std::bind" << endl;
	std::vector<std::string> vec7{"As", "The", "Ended", "End"};
	const size_t sz2{1};
	const size_t sx{3};
	auto it_longer2 = std::find_if(vec7.cbegin(), vec7.cend(), std::bind(isPrint, _1, sz2, sx));
	if(it_longer2 != vec7.cend())
		cout << "Find word: " << *it_longer2 << endl;
	/// iterators
	cout << endl << "iterators" << endl;
	/// итераторы вставки
	std::list<int> lst{1, 2, 3, 4, 5, 5, 7, 8, 9};
	std::list<int> lst1, lst2;
	for(const auto& elem : lst)
		cout << elem << " ";
	cout << endl;
	std::copy(vec.cbegin(), vec.cend(), front_inserter(lst1));
	for(const auto& elem : lst1)
		cout << elem << " ";
	cout << endl;
	std::copy(vec.cbegin(), vec.cend(), inserter(lst2, lst2.begin()));
	for(const auto& elem : lst2)
		cout << elem << " ";
	cout << endl;
	/// потоковый итеартор ввода
//	std::istream_iterator<int> in_iter(cin), eof;
//	std::vector<int> vec9(in_iter, eof);
//	std::vector<int> vec10(in_iter, eof); // при чтении из одного потока в два объекта нужно синхронизировать чтение из потока (пока не знаю как)
//	cout << "vec9: ";
//	for(const auto& elem : vec9)
//		cout << elem << " ";
//	cout << endl;
//	cout << "vec10: ";
//	for(const auto& elem : vec10)
//		cout << elem << " ";
//	cout << endl;
	// требуется синхронизация чтения из потока
//	std::istream_iterator<int> in_iter1(cin), eof1;
//	int istream_sum = std::accumulate(in_iter1, eof1, 0);
//	cout << "istream_sum = " << istream_sum << endl;
	/// потоковый итеартор вывода
//	std::ostream_iterator<int> os_iterator(cout, " ");
//	for(const auto& elem : vec9)
//		*os_iterator++ = elem;
//	cout << endl;
//	for(const auto& elem : vec9)
//		os_iterator = elem;
//	cout << endl;
//	std::copy(vec9.cbegin(), vec9.cend(), os_iterator);
//	cout << endl;
	/// реверсивный итератор
	std::string str{"Hello, how are you, Andrey?"};
	auto word_it = std::find(str.crbegin(), str.crend(), ',');
	cout << std::string(word_it.base(), str.cend()) << endl;
	cout << std::string(++word_it.base(), --str.cend()) << endl;
	for(auto lst_beg = lst.crbegin(); lst_beg != lst.crend(); ++lst_beg)
		cout << *lst_beg << " ";
	cout << endl;
	std::list<int> lst3{1, 2, 3, 4, 5, 5, 7, 8, 9};
	std::list<int> lst4;
	// lst3 без изменений
	std::merge(lst3.begin(), lst3.end(), lst4.begin(), lst4.end(), std::back_inserter(lst4));
	for(const auto& elem : lst4)
		cout << "lst4: " << elem << " ";
	cout << endl;
	for(const auto& elem : lst3)
		cout << "lst3: " << elem << " ";
	cout << endl;
	// из lst3 удаляются элементы
	lst4.merge(lst3);
	for(const auto& elem : lst4)
		cout << "lst4: " << elem << " ";
	cout << endl;
	for(const auto& elem : lst3)
		cout << "lst3: " << elem << " ";
	cout << endl;



	return 0;
}
